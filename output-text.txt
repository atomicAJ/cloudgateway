{segment}
spring:
  cloud:
    gateway:
      routes:
      - predicates:
        - Path=/checkout-service/{segment}
        uri: http://localhost:9022/
        filters:
        - StripPrefix=1
		- AddRequestParameter=foo, {segment}
        - TestRequest