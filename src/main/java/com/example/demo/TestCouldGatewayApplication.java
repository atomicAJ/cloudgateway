package com.example.demo;

import java.security.Principal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpStatus;
import com.example.demo.utils.TestBody;
import reactor.core.publisher.Mono;

@SpringBootApplication
public class TestCouldGatewayApplication {

	private static final String BACKSLASH = "/";
	
	@Autowired
	TestBody tBody;

	public static void main(String[] args) {
		SpringApplication.run(TestCouldGatewayApplication.class, args);
	}	

	@Bean
	public GlobalFilter customGlobalFilter() {
		return (exchange, chain) -> exchange.getPrincipal().map(Principal::getName).defaultIfEmpty("Default User")
				.map(userName -> {
					System.out.println("Global Pre filter invoked");
					//resolveBodyFromRequest(exchange);
					// adds header to proxied request
					// exchange.getRequest().mutate().header("CUSTOM-REQUEST-HEADER",
					// userName).build();
					return exchange;
				}).flatMap(chain::filter);
	}

	@Bean
	public GlobalFilter customGlobalPostFilter() {
		return (exchange, chain) -> chain.filter(exchange).then(Mono.just(exchange)).map(serverWebExchange -> {
			// adds header to response
			if (serverWebExchange.getResponse().getStatusCode() == HttpStatus.OK) {
				System.out.println("Global Post Filter Invoked");
				String event = serverWebExchange.getRequest().getPath().toString().replaceAll(BACKSLASH, "");
				System.out.println("Body from request###### : "+tBody.getBody());
			}
			return serverWebExchange;
		}).then();
	}

}
