
package com.example.demo.filters;

import org.springframework.cloud.gateway.filter.GatewayFilter;
import org.springframework.cloud.gateway.filter.factory.GatewayFilterFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

@Component
public class MyCustomPostFilter implements GatewayFilterFactory {

	@Override
	public GatewayFilter apply(Object config) {
		return (exchange, chain) -> {
			return chain.filter(exchange).then(Mono.fromRunnable(() -> {
				ServerHttpResponse response = exchange.getResponse();
				if (response.getStatusCode().equals(HttpStatus.OK)) {
					System.out.println("%%%%%%%%%%%%%%%MyFilter Invoked%%%%%%%%%%%");
					exchange.getRequest().getURI();
				}
			}));
		};
	}

	@Override
	public Config newConfig() {
		return new Config("MyFilter");
	}

	public static class Config {

		public Config(String name) {
			this.name = name;
		}

		private String name;

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}
	}

}
