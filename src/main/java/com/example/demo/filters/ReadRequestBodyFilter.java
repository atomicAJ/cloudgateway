
package com.example.demo.filters;

import java.net.URI;
import java.nio.CharBuffer;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.atomic.AtomicReference;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.gateway.filter.GatewayFilter;
import org.springframework.cloud.gateway.filter.factory.GatewayFilterFactory;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.core.io.buffer.DataBufferUtils;
import org.springframework.core.io.buffer.NettyDataBufferFactory;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpRequestDecorator;
import org.springframework.stereotype.Component;
import org.springframework.web.util.UriComponentsBuilder;

import com.example.demo.utils.TestBody;

import io.netty.buffer.ByteBufAllocator;
import reactor.core.publisher.Flux;

@Component
public class ReadRequestBodyFilter implements GatewayFilterFactory {

	@Autowired
	TestBody tBody;

	@Override
	public GatewayFilter apply(Object config) {
		return (exchange, chain) -> {
			URI uri = exchange.getRequest().getURI();
			System.out.println("**********Request URL*******" + uri);
			URI ex = UriComponentsBuilder.fromUri(uri).build(true).toUri();
			ServerHttpRequest request = exchange.getRequest();
			System.out.println("Response Code: ********" + exchange.getResponse().getStatusCode());
			System.out.println("**********Post Result*******");
			Flux<DataBuffer> body = exchange.getRequest().getBody();
			AtomicReference<String> bodyRef = new AtomicReference<>();
			body.subscribe(buffer -> {
				CharBuffer charBuffer = StandardCharsets.UTF_8.decode(buffer.asByteBuffer());
				DataBufferUtils.release(buffer);
				bodyRef.set(charBuffer.toString());
			});
			String data = bodyRef.get();
			tBody.setBody(data);
			System.out.println("Body in My filter: " + data);
			DataBuffer bodyDataBuffer = stringBuffer(data);
			Flux<DataBuffer> bodyFlux = Flux.just(bodyDataBuffer);

			request = new ServerHttpRequestDecorator(request) {
				@Override
				public Flux<DataBuffer> getBody() {
					return bodyFlux;
				}
			};
			return chain.filter(exchange.mutate().request(request).build());
		};
	}

	@Override
	public Config newConfig() {
		return new Config("MyFilter");
	}

	public static class Config {

		public Config(String name) {
			this.name = name;
		}

		private String name;

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}
	}

	protected DataBuffer stringBuffer(String value) {
		byte[] bytes = value.getBytes(StandardCharsets.UTF_8);

		NettyDataBufferFactory nettyDataBufferFactory = new NettyDataBufferFactory(ByteBufAllocator.DEFAULT);
		DataBuffer buffer = nettyDataBufferFactory.allocateBuffer(bytes.length);
		buffer.write(bytes);
		return buffer;
	}

}
