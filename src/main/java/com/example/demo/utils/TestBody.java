package com.example.demo.utils;

import org.springframework.stereotype.Service;

@Service
public class TestBody {
	String body;

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

}
